$(document).ready(function() {
	if (typeof addthis != "undefined") {
		addthis.layers({
			'theme': 'transparent',
			'share': {
				'position': 'left',
				'services': 'facebook,twitter,google_plusone_share,pinterest',
				'offset': {
					'top': '230px'
				}
			},
			'thankyou': 'false',
			'responsive': {
				'maxWidth': '768px'
			},
			'mobile': {
				'buttonBarPosition': 'bottom',
				'buttonBarTheme': 'light',
				'mobile': true
			}
		});
	}
});
