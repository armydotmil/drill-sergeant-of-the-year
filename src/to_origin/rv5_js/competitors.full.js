var competitor=new Array(6)
competitor[0]="";
competitor[1]="";
competitor[2]="";
competitor[3]="";
competitor[4]="";
competitor[5]="";

// Competitor
competitor[0] = '<a href="competitors.html?tab=0"><img class="bioPic_sm" src="http://usarmy.vo.llnwd.net/e2/rv5_images/drillsergeant/competitors/kohoutek_sm.jpg" alt="Staff Sgt. Jasper R. Kohoutek"/></a>\n';
competitor[0] += '<h3 class="sergeant">Staff Sergeant</h3>\n';
competitor[0] += '<h4 class="name">Jasper R. Kohoutek</h4>\n';
competitor[0] += '<p>Drill Sergeant Jasper Kohoutek was born in Coeur d\'Alene, ID. He graduated from Post Falls High School, Post Falls, ID. He then went on and graduated from North Idaho College with an Applied Science Degree in HVAC in 1996. He entered the United States Army in November 2003. He completed Infantryman One Station Unit Training at Fort Benning, Ga. in March 2004.</p>\n';
competitor[0] += '<p>Kohoutek\'s assignments include: Bravo Company, 1st Battalion, 505th Parachute Infantry Regiment, Fort Bragg, N.C. and Alpha Company, 3rd Battalion, 415th Regiment, 95th Division, Spokane, Wash. He has also deployed in support of Operation Enduring Freedom and Operation Iraqi Freedom.</p>\n';
competitor[0] += '<a href="competitors.html">View All Competitors</a>';

// Competitor
competitor[1] = '<a href="competitors.html?tab=1"><img class="bioPic_sm" src="http://usarmy.vo.llnwd.net/e2/rv5_images/drillsergeant/competitors/palmer_sm.jpg" alt="Staff Sgt. Andrew J. Palmer"/></a>\n';
competitor[1] += '<h3 class="sergeant">Staff Sergeant</h3>\n';
competitor[1] += '<h4 class="name">Andrew J. Palmer</h4>\n';
competitor[1] += '<p>Drill Sergeant Andrew Palmer was born in Milwaukee, Wisc. He graduated from Kewaskum High School in Kewaskum, Wisc. and entered the United States Army in June of 1997. He completed Infantry One Station Unit Training at Ft. Benning, Ga. in November 1997.</p>\n';
competitor[1] += '<p>Palmer\'s assignments include: Delta Company, 2nd Battalion 505th Parachute Infantry Regiment, Fort Bragg, N.C.; 1454th Transportation Company, Concord, N.C.; Charlie Company, 3rd Battalion 518th Regiment, Hickory, N.C. Palmer has deployed in support of Operation Iraqi Freedom and Support Group Haiti-Port Au Prince, Haiti. </p>\n';
competitor[1] += '<a href="competitors.html">View All Competitors</a>';

// Competitor
competitor[2] = '<a href="competitors.html?tab=2"><img class="bioPic_sm" src="http://usarmy.vo.llnwd.net/e2/rv5_images/drillsergeant/competitors/heslin_sm.jpg" alt="Staff Sgt. John Heslin"/></a>\n';
competitor[2] += '<h3 class="sergeant">Staff Sergeant</h3>\n';
competitor[2] += '<h4 class="name">John Heslin</h4>\n';
competitor[2] += '<p>Drill Sergeant Heslin was born in Baltimore, Md. He graduated from St. John�s at Prospect Hall High School in Frederick, Md. and entered the United States Army in July 2004. He completed Fire Support Specialist One Station Unit Training at Fort Sill, OK in October 2004.</p>\n';
competitor[2] += '<p>Heslin\'s assignments include: Headquarters Company, 1st Battalion, 12th Infantry Regiment, Fort Hood, Texas; Headquarters Company, 2nd Battalion, 7th Cavalry Regiment, Fort Hood. His deployments include: Operation Iraqi Freedom 05-07 Task Force Cobra, Operation Iraqi Freedom 08-10 Task Force Mountain.</p>\n';
competitor[2] += '<a href="competitors.html">View All Competitors</a>';

// Competitor
competitor[3] = '<a href="competitors.html?tab=3"><img class="bioPic_sm" src="http://usarmy.vo.llnwd.net/e2/rv5_images/drillsergeant/competitors/frailey_sm.jpg" alt="Staff Sgt. Felicia D. Frailey"/></a>\n';
competitor[3] += '<h3 class="sergeant">Staff Sergeant</h3>\n';
competitor[3] += '<h4 class="name">Felicia D. Frailey</h4>\n';
competitor[3] += '<p>Drill Sergeant Frailey was born in Altus, Okla. She graduated from Altus High School in May 2000. In December 2004, she enlisted in the United States Army. She completed Engineer One Station Unit Training at Fort Leonard Wood, Mo. in April 2005.</p>\n';
competitor[3] += '<p>Frailey\'s assignments include: 74th Multi Role Bridging Company (MRBC), 36th Engineer Brigade, Fort Hood, Texas; Charlie Battery, 1-40th Field Artillery Battalion, Fort Sill, Okla. She has deployed in support of Operation Iraqi Freedom from August 2005 to August 2006 and Operation Iraqi Freedom from November 2007 to February 2009.</p>\n';
competitor[3] += '<a href="competitors.html">View All Competitors</a>';

// Competitor
competitor[4] = '<a href="competitors.html?tab=4"><img class="bioPic_sm" src="http://usarmy.vo.llnwd.net/e2/rv5_images/drillsergeant/competitors/facio_sm.jpg" alt="Staff Sgt. Benjamin Facio"/></a>\n';
competitor[4] += '<h3 class="sergeant">Staff Sergeant</h3>\n';
competitor[4] += '<h4 class="name">Benjamin Facio</h4>\n';
competitor[4] += '<p>Drill Sergeant Benjamin Facio was born in Lakewood, Calif. and graduated from Cypress High School in Cypress, Calif. in 2001. In 2004, he joined the United States Army and attended Basic Combat Training and Military Police training at Fort Leonard Wood, Mo.</p>\n';
competitor[4] += '<p>Following Facio\'s Initial Entry Training with Charlie Company, 795th Military Police Battalion, he served in assignments stateside including the 561st Military Police Company, 716th Military Police Battalion at Fort Campbell, Ky.; the National Training Center Police Company, Headquarter and Headquarters Company at Fort Irwin, Calif.; Echo Company, 787th Military Police Battalion, Fort Leonard Wood. His duty positions include Patrol Supervisor, Team Leader, Squad Leader and Drill Sergeant.</p>\n';
competitor[4] += '<a href="competitors.html">View All Competitors</a>';

// Competitor
competitor[5] = '<a href="competitors.html?tab=5"><img class="bioPic_sm" src="http://usarmy.vo.llnwd.net/e2/rv5_images/drillsergeant/competitors/goscinski_sm.jpg" alt="Staff Sgt. Samantha Goscinski"/></a>\n';
competitor[5] += '<h3 class="sergeant">Staff Sergeant</h3>\n';
competitor[5] += '<h4 class="name">Samantha Goscinski</h4>\n';
competitor[5] += '<p>Drill Sergeant Goscinski was born in Seattle, Wash. She graduated from Evergreen Senior High School, Seattle, Wash., and enlisted in the United States Army in October 2003. She completed One Station Unit Training at Fort Leonard Wood, Mo. in February 2004.</p>\n';
competitor[5] += '<p>Goscinski\'s assignments include: 188th Military Police Company, Camp Walker, Daegu, South Korea; 17th Military Police Detachment, Fort Jackson, South Carolina; 272nd Military Police Company, Barton Barracks, Ansbach, Germany; 382nd Military Police Company, Bagram Airfield, Afghanistan; 527th Military Police Company, Barton Barracks, Ansbach, Germany; Echo Company, 3rd Battalion, 34th Infantry Regiment, Fort Jackson, South Carolina. Goscinski has deployed in support of Operation Enduring Freedom VIII.</p>\n';
competitor[5] += '<a href="competitors.html">View All Competitors</a>';




function get_random()
{
    var ranNum= Math.floor(Math.random()*6);
    return ranNum;
}
var random = get_random();
document.getElementById('shortBio').innerHTML += competitor[random];