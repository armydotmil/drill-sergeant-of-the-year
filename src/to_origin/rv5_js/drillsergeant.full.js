/*Last Modified 2014-12-02 1015 BMN */
$(document).ready(function() {

//----- image slideshow components on homepage and competition page -----// 
    $('#banner, #dsoyBanner').cycle({ 
        fx:     'fade',
        speed:   2000,
        timeout: 4000
    });

//----- more stories -----//
    var display = 4;
    var storyLength = $(".storyBlock").length;
    $(".storyBlock").hide();
    $(".storyBlock:lt("+display+")").show();
    
    $("#archives").click(function(){
        if (display < storyLength){
            display = display+4;
            $(".storyBlock:lt("+display+")").show();
        }else{
            $("#archives").hide();
        }
        return false;
    });

//----- ie navigation fix -----//   
    $("ul#mainNav li:nth-child(1)").css("margin-right", "60px");
    $("ul#mainNav li:nth-child(2)").css("margin-right", "316px");
    $("ul#mainNav li:nth-child(3)").css("margin-right", "60px");
    $("ul#mainNav li:nth-child(4)").css("margin-right", "0px");

//----- Expandable links -----//
    $('#competition-accordion .accordion-headers').click(function() {
        $(this).next().toggle('slow');
        $(this).children("span.icon").toggleClass('up-arrow-icon down-arrow-icon');
        return false;
    }).next().hide();

});// end of js